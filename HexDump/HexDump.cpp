// HexDump
// v1.02

#include <iostream>		// cout
#include <fstream>		// ifstream
#include <iomanip>		// stream modifiers (such as setfill, setw)
#include <string>
#include <vector>

using namespace std;

void dumpFile(ifstream &inFile1);
void dumpLine(char *buffer, streamsize bytesRead, int &offset);
bool processCommandLine(int argc, const char* argv[]);
void showUsage();
bool isPositiveInteger(string value);
void displayOffset(int &offset);
void displayHex(char *buffer, streamsize bytesRead);
void displayAscii(char *buffer, streamsize bytesRead);

#define DEFAULT_WIDTH 32
int width = DEFAULT_WIDTH;
bool isDisplayOffset = true;
bool isDisplayHex = true;
bool isDisplayAscii = true;
bool isUpperCase = true;
char unprintableChar = ' ';
string filename = "";

int main(int argc, const char* argv[]) {

    if (processCommandLine(argc, argv)) {
	ifstream inFile1;
	inFile1.open(filename.c_str(), ifstream::binary);
	if (inFile1.is_open()) {
	    dumpFile(inFile1);
	} else {
	    cout << "Couldn't open input file " << filename << '\n';
	}
    }

    return 0;
}

void dumpFile(ifstream &inFile1) {
    // dump each line to the console
    // until end of file reached

    char *buffer = new char[width];
    int offset = 0;
    while (inFile1) {
	inFile1.read(buffer, width);
	streamsize bytesRead = inFile1.gcount();
	if (bytesRead > 0) {
	    dumpLine(buffer, bytesRead, offset);
	    offset += (int) bytesRead;
	}
    }

}

void dumpLine(char *buffer, streamsize bytesRead, int &offset) {
    // dump a single line to the console

    if (isDisplayOffset)
	displayOffset(offset);

    if (isDisplayHex)
	displayHex(buffer, bytesRead);

    cout << "     "; // space between hex and ascii

    if (isDisplayAscii)
	displayAscii(buffer, bytesRead);

    cout << '\n';
}

void displayOffset(int &offset) {
    cout << "0x" << hex << setfill('0') << setw(8) << offset << "    ";
}

void displayHex(char *buffer, streamsize bytesRead) {
    for (int i = 0; i < bytesRead; i++) {
	cout << hex << (isUpperCase ? uppercase : nouppercase) << setfill('0') << setw(2) << (buffer[i] & 0xff) << ' ';
    }
    // add spaces in place of characters missing due to not reading WIDTH bytes
    cout << setfill(' ') << setw((width - bytesRead) * 3) << "";
}

void displayAscii(char *buffer, streamsize bytesRead) {
    for (int i = 0; i < bytesRead; i++) {
	cout << ((buffer[i] < ' ' || buffer[i] > 'z') ? unprintableChar : buffer[i]);
    }
}

bool processCommandLine(int argc, const char* argv[]) {

    // put command line arguments into vector, using the range constructor 
    // enabling a range-based for loop. skip first argument which is the executable name
    vector<string> args(argv + 1, argv + argc);

    for (const string &parameter : args) {

	// if first character is a '-' alter options accordingly based on second character
	if (parameter[0] == '-') {

	    // switch on the second character of the parameter
	    switch (parameter[1]) {

		case '?':
		{
		    showUsage();
		    return false;
		    break;
		}

		case 'o':
		{
		    isDisplayOffset = false;
		    break;
		}
		case 'h':
		{
		    isDisplayHex = false;
		    break;
		}
		case 'a':
		{
		    isDisplayAscii = false;
		    break;
		}
		case 'c':
		{
		    isUpperCase = false;
		    break;
		}

		case 'w':
		{
		    string swidth = parameter.substr(2);
		    if (!isPositiveInteger(swidth) || (width = stoi(swidth)) < 1) {
			cout << "Error: width must be an integer greater than 0\n";
			return false;
		    }
		    break;
		}

		case 'u':
		{
		    if (parameter.size() < 3) {
			cout << "Error: unprintable character must follow -u switch\n";
			return false;
		    }
		    unprintableChar = parameter[2];
		    break;
		}

		default:
		{
		    cout << "Error: unrecognized option -" << parameter[1] << '\n';
		    showUsage();
		    return false;
		}
	    }
	} else {
	    // else it's the filename
	    // don't process any further parameters
	    filename = parameter;
	    break;
	}
    }

    if (filename == "") {
	cout << "Error: no input file specified" << "\n\n";
	showUsage();
	return false;
    }

    if (!(isDisplayAscii || isDisplayHex)) {
	cout << "Error: You must display either hex or ascii\n";
	showUsage();
	return false;
    }

    return true;

}

bool isPositiveInteger(string value) {
    return (value.length() > 0 && value.find_first_not_of("0123456789") == string::npos);
}

void showUsage() {

    cout << "HexDump   Dumps a hex and ascii representation of a file to the console.\n";
    cout << "Usage:    HexDump [-w] [-o] [-h] [-a] [-u] [-c] filename\n";
    cout << "-wn       Specify the output width. The default is " << (DEFAULT_WIDTH) << ".\n";
    cout << "-o        Don't display the offset column.\n";
    cout << "-h        Don't display hex representation.\n";
    cout << "-a        Don't display ascii representation.\n";
    cout << "-uchar    Specify the character to be used to represent unprintable characters in the ascii column. Default is ' '.\n";
    cout << "-c        Display hex digits A-F in lower case, rather than the default upper case.\n";

}
